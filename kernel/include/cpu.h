#ifndef _CPU_H
#define _CPU_H
	
#include "stdint.h"
	
struct cpu_state
	
{
	
   
    uint32_t usr_r13, usr_r14;
   
    uint32_t cpsr, svc_spsr;

    uint32_t svc_r13, svc_r14;
 
    uint32_t r0, r1, r2, r3, r4, r5, r6, r7;	
    uint32_t r8, r9, r10, r11, r12, r15;	
} __attribute__((packed));		
#endif
