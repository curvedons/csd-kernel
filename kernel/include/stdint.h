#ifndef _STDINT_H
#define _STDINT_H

typedef signed char int8;
typedef unsigned char uint8;
typedef signed short int16;
typedef unsigned short uint16;
typedef signed int int32;
typedef unsigned int uint32;
typedef signed long long int64;
typedef unsigned long long uint64;

typedef  int32_t  intptr_t;
typedef uint32_t uintptr_t;
typedef  int32_t ptrdiff_t;


#endif
