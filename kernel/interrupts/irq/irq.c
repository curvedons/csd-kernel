#include "../../include/cpu.h"
#include "../../include/stdint.h"

#define IC_REGS *((volatile uint32 *)0x14000000)

void init_irqs(){
 IC_REGS[3]=0xFFFFFFFF;
 
     __asm__ __volatile__ ("mrs r0,cpsr;"
                          "bic r0,#0x80;"
                          "msr cpsr_ctl,r0" ::: "r0");    
     
}



statuc struct cpu_state *(*irq_handlers[32])(struct cpu_state *state);

void register_irq_handler(unsigned irq, struct cpu_state (*handler)(struct cpu_state *state))
{
 if (irq >= 32){
 return;        
}    



if(irq_handlers[irq]){
        return;              
}

irq_handlers[irq]=handler;
IC_REGS[2] |= 1 << irq;
     
}

struct cpu_state *handle_irq(struct cpu_state *state)	
{		
    unsigned irq = 0;	
    uint32_t status = IC_REGS[0];		
    while (status)	
    {	
        if (status & 1)	
            if (irq_handlers[irq])	
                state = irq_handlers[irq](state);	
        irq++;	
        status >>= 1;	
}	
    return state;
}
